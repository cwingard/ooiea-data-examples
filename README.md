# OOI Endurance Array Dataset Parsing and Processing

Collection of example notebooks detailing how to access, download, parse and process data from the OOI Endurance Array. Provided examples work with data from the [raw source files](https://rawdata.oceanobservatories.org/files/) or from the [OOI Data Portal](https://ooinet.oceanobservatories.org/).

The goal is to provide users interested in using data from the OOI Endurance Array with the guidance and tools they need to approach and begin working with the data. These tools are applicable to the OOI Pioneer and Global Arrays as well. With some additional work, they could be extended to the Cabled Array.

## Setup

Utilizing the [Anaconda](https://www.anaconda.com) python tools, create a local environment with the needed tools for parsing and processing the OOI Endurance Array data starting from the source data files. The setup creates an environment with a full suite of the python tools needed to not only work with the OOI Endurance Array data, but includes several additional tools for working with whatever scientific data you might choose to explore. See the [download and installation](https://www.anaconda.com/download/) instructions to obtain a copy of [Anaconda](https://www.anaconda.com). After you have installed Anaconda, install this repo and the example notebooks by cloning it to your local machine, or just copy/download the environment.yml file from this repo to your local machine in a directory of your choice. From that point, via either the Anaconda Prompt (via the start menu) or your favorite shell:  

```bash
cd <dir_with_environment.yml_file>  
conda create -n ooiea python=3.6 anaconda  
conda activate ooiea  
conda env update -f environment.yml  
```

Questions, comments, criticisms? Contact us at [ooice.platforms@gmail.com](mailto:ooice.platforms@gmail.com)